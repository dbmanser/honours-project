
import csv
import sys

range_file = sys.argv[1] # 5 genomic regions ID
regions_file = sys.argv[2] # 7 merged...

'''Load all the data into memory so we can do a search'''
ranges = []
with open(range_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        ranges.append(row)

'''End load data. Real work beings below:'''
        
with open(regions_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for region in reader:
        cord1 = int(region[1])
        cord2 = int(region[2])
        
        for range_row in ranges:
            lower = int(range_row[1])
            upper = int(range_row[2])
            
            if range_row[0] == region[0] and ((cord1 >= lower and cord1 <= upper) or (cord2 >= lower and cord2 <= upper) or (lower >= cord1 and cord2 >= upper)):
                locus = range_row[3]
                out = [region[0],region[1],region[2],region[3],region[4],region[5],region[6],locus]
                print "\t".join(out)

            