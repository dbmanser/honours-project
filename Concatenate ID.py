import csv
import sys

first_arg = sys.argv[1]

with open(first_arg, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        locus = row[0] + '\t' +row[1]+'\t'+row[2]+'\t'+"ID:"+row[3]+":"+row[4]+"-"+row[5]
        print locus
