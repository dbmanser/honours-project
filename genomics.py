
f = open('Chr4 Region 4-2', 'r')

# Locus name -> data
locus = {}

'''Loads each locus into the locus dictionary keyed by locus name'''
def process_line(line):
    if not line.find('#') is 0:
        # Remove the line break \n and split by tabs
        columns = line.replace('\n','').split('\t')
        locus_name = columns[1]
        locus[locus_name] = columns[2:]

# Processes the file
for line in f:
    process_line(line)
    
# Your code here to process data in the locus array
# print locus['ENST00000345009.4']

results_by_locus = {}

for name,locus_data in locus.iteritems():
    print name
    print locus_data
    
    if (locus_data #...define test for the data):
        data = #extract your data
        results = results_by_locus[name]
        if not results:
            results = []
        results.append(data)
        results_by_locus[name]
