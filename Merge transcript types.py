
import csv
import sys

first_arg = sys.argv[1]

genes = {}
gene_to_transcript_types = {}

with open(first_arg, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        chr_number = row[2]
        pos_start = row[4]
        pos_end = row[5]
        gene_name = row[12].strip()
        gene_type = row[18]
        transcript_type = row[22].strip()
        transcription_level = row[30]
        
        if gene_name not in gene_to_transcript_types:
            gene_to_transcript_types[gene_name] = []
        gene_to_transcript_types[gene_name].append(transcript_type)
        
        genes[gene_name] = [chr_number, pos_start, pos_end, gene_name, gene_type, None, transcription_level]
        
    for key,value in genes.iteritems():
        row = genes[key]
        transcript_types = gene_to_transcript_types[key]
        row[5] = '; '.join(set(transcript_types))
        print '\t'.join(row)
