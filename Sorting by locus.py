
import csv
import sys

dictionary_file = sys.argv[1]
summary_file = sys.argv[2]

categories = {}
with open(dictionary_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        categories[row[0]] = row[1] 


loci_group = {}
with open(summary_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        locus = row[7]
        if not locus in loci_group:
            loci_group[locus] = []
        loci_group[locus].append(row)

def to_csv(data):
    return ', '.join(set(data))

for locus,loci in loci_group.iteritems():
    protein_coding = []
    lincRNA = []
    antisense = []
    long_nc_rna = []
    small_nc_rna = []
    pseudogen = []
    rrna = []
    
    for row in loci:
        name = row[3]
        transcript_types = row[5]
        category = categories[transcript_types]
        if category == 'protein_coding':
            protein_coding.append(name)
        elif category == 'lincRNA':
            lincRNA.append(name)
        elif category == 'antisense':
            antisense.append(name)
        elif category == 'lncRNA':
            long_nc_rna.append(name)
        elif category == 'small ncRNA':
            small_nc_rna.append(name)
        elif category == 'pseudogene':
            pseudogen.append(name)
        elif category == 'rRNA':
            rrna.append(name)
        else:
            raise 'Not handled ' + category
    print "\t".join([locus,to_csv(protein_coding), to_csv(lincRNA), to_csv(antisense), to_csv(long_nc_rna), to_csv(small_nc_rna),to_csv(pseudogen),to_csv(rrna)])
    
